package de.batalski.portlet2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Created by user1 on 14.07.2016.
 */
@Controller
@RequestMapping("VIEW")
public class HelloPortletViewController {
    @RenderMapping
    public String sayHello(Model model) {
        model.addAttribute("name", "Gena");
        return "hello";
    }
}
