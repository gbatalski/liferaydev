package net.rgielen.elasgfu.portlet;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloGenericPortlet extends GenericPortlet {

    @Override
    protected void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        request.setAttribute("foo", new Object());
        request.setAttribute("wstate", request.getWindowState().toString());
        getPortletContext().getRequestDispatcher("/view.jsp?bar=foo").include(request, response);
    }

    @Override
    protected void doHelp(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        final PrintWriter out = response.getWriter();
        out.println("Generic simple Portlet Help Page");
        showIds(request, response, out);
        out.println("<div id=\""+response.getNamespace()+"myAjaxDiv1\"></div>");
        out.println("<div id=\"myAjaxDiv2\"></div>");
    }

    private void showIds(RenderRequest request, RenderResponse response, PrintWriter out) {
        out.println("<p> Namespace: "+response.getNamespace()+"</p>");
        out.println("<p> WindowID: "+request.getWindowID()+"</p>");
    }
}
