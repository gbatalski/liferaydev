package net.rgielen.elasgfu.portlet;

import javax.portlet.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloPortlet implements Portlet {

    private ResourceBundle deBundle;
    private ResourceBundle enBundle;
    private PortletConfig config;

    @Override
    public void init(PortletConfig config) throws PortletException {
        deBundle = config.getResourceBundle(Locale.GERMANY);
        enBundle = config.getResourceBundle(Locale.UK);
        this.config = config;
    }

    @Override
    public void processAction(ActionRequest request, ActionResponse response) throws PortletException, IOException {
    }

    @Override
    public void render(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        final PrintWriter out = response.getWriter();
        out.println("<h1>Hello World!</h1>");
    }

    @Override
    public void destroy() {
    }
}
