package net.rgielen.elasgfu.portletapp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Configuration
@ComponentScan
@EnableJpaRepositories
public class PortletAppConfig {
}
