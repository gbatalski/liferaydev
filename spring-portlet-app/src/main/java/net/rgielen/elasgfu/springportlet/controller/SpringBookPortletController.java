package net.rgielen.elasgfu.springportlet.controller;

import net.rgielen.elasgfu.portletapp.entity.Book;
import net.rgielen.elasgfu.portletapp.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Controller
@RequestMapping("VIEW")
public class SpringBookPortletController {

    @Autowired
    BookRepository bookRepository;

    @RenderMapping
    public String renderList(Model model) {
        model.addAttribute("bookList", bookRepository.findAll());
        return "list";
    }

    @ActionMapping(name = "addBook")
    public void newItemAction(ActionResponse response) {
        response.setRenderParameter("view", "newBook");
    }

    @RenderMapping(params = "view=newBook")
    public String renderNewBookForm(Model model) {
        model.addAttribute("book", new Book());
        return "edit";
    }

    @ActionMapping(name = "saveBook")
    public void saveBookAction(@ModelAttribute("book") Book book, ActionResponse response) {
        final Book saved = bookRepository.save(book);
        response.setRenderParameter("view", "editBook");
        response.setRenderParameter("id", "" + saved.getId());
    }

    @RenderMapping(params = "view=editBook")
    public String renderBookBookForm(Model model, @RequestParam Integer id) {
        model.addAttribute("book", bookRepository.findOne(id));
        return "edit";
    }

}
