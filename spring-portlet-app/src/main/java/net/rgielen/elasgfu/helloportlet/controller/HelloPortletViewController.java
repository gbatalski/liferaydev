package net.rgielen.elasgfu.helloportlet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Controller
@RequestMapping("VIEW")
public class HelloPortletViewController {

    @RenderMapping
    public String sayHello() {
        return "hello";
    }
}
