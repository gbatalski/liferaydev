package de.batalski.app.entity;

import javax.persistence.Entity;

/**
 * Created by user1 on 14.07.2016.
 */
@Entity
public class Book {
    private Integer id;
    private String isbn;
    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
