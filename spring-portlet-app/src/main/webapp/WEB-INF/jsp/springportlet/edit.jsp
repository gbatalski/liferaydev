<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<portlet:defineObjects/>

<h1>Edit Book</h1>

<portlet:actionURL var="saveUrl" name="saveBook"/>
<portlet:renderURL var="listUrl" portletMode="VIEW"/>
<form:form method="post" modelAttribute="book" action="${saveUrl}">
    <form:hidden path="id"/>
    <form:label path="title">Titel</form:label>
    <form:input path="title"/>
    <form:label path="isbn">ISBN</form:label>
    <form:input path="isbn"/>
    <input type="submit" value="OK"/>
</form:form>
<a href="${listUrl}">Back to List</a>
