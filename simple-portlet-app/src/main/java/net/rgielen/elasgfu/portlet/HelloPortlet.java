package net.rgielen.elasgfu.portlet;

import javax.portlet.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloPortlet implements Portlet {

    public static final String ACTION_CALLED = "actionCalled";

    private ResourceBundle deBundle;
    private ResourceBundle enBundle;
    private PortletConfig config;

    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void init(PortletConfig config) throws PortletException {
        deBundle = config.getResourceBundle(Locale.GERMANY);
        enBundle = config.getResourceBundle(Locale.UK);
        this.config = config;
    }

    @Override
    public void processAction(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        response.setRenderParameter(ACTION_CALLED, "true");
        System.out.println("foo parameter in processAction: " + request.getParameter("foo"));
    }

    @Override
    public void render(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        final PortletURL actionURL = response.createActionURL();
        actionURL.setParameter("foo", "bar");
        actionURL.setWindowState(WindowState.MAXIMIZED);

        System.out.println("foo parameter in render: " + request.getParameter("foo"));

        final String actionCalledParameter = request.getParameter(ACTION_CALLED);

        final PortletURL renderURL = response.createRenderURL();
        renderURL.setWindowState(WindowState.MAXIMIZED);
        renderURL.setParameter("foo", "bar");
        if (actionCalledParameter != null) {
            renderURL.setParameter(ACTION_CALLED, actionCalledParameter);
        }

        final PortletMode portletMode = request.getPortletMode();
        final PrintWriter out = response.getWriter();
        if (portletMode.equals(PortletMode.HELP)) {
            out.println("Help for Hello World");
        } else {
            out.println("<h1>Hello World!</h1>");

            // Action URL example
            out.println("<p><a href=\"");
            actionURL.write(out);
            out.println("\">To Action!</a></p>");

            // Render URL example
            out.println("<p><a href=\"");
            renderURL.write(out);
            out.println("\">Re-render maximized!</a></p>");

            renderNavUrls(out, request, response);

            if ("true".equals(actionCalledParameter)) {
                out.println("<p>Action was called!</p>");
            }

            out.println("<p>"+counter.addAndGet(1)+"</p>");
            out.println("<p> Namespace: "+response.getNamespace()+"</p>");
            out.println("<p> WindowID: "+request.getWindowID()+"</p>");
        }
    }

    private void renderNavUrls(PrintWriter out, RenderRequest request, RenderResponse response) throws IOException {
        out.println("<p><a href=\"");
        PortletURL newURL = response.createActionURL();
        newURL.setParameter("action", "new");
        newURL.write(out);
        out.println("\">Create new Item</a></p>");

        out.println("<p><a href=\"");
        PortletURL editURL = response.createActionURL();
        editURL.setParameter("action", "edit");
        editURL.write(out);
        out.println("\">Edit Item</a></p>");

        out.println("<p><a href=\"");
        PortletURL viewURL = response.createActionURL();
        viewURL.setParameter("action", "view");
        viewURL.write(out);
        out.println("\">View Item</a></p>");
    }

    @Override
    public void destroy() {
    }
}
