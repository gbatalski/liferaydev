package net.rgielen.elasgfu.portletapp.repository;

import net.rgielen.elasgfu.portletapp.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface BookRepository extends JpaRepository<Book, Integer> {
}
