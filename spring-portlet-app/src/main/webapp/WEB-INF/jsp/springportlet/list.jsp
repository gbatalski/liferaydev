<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<portlet:defineObjects />

<h1>Books</h1>
<ul>
<c:forEach items="${bookList}" var="book">
    <li>${book.id} - <c:out value="${book.title}"/></li>
</c:forEach>
</ul>

<p/>

<portlet:actionURL var="newUrl" name="addBook"/>
<a href="${newUrl}">Add new Book</a>