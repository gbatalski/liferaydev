package net.rgielen.elasgfu.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String user = req.getParameter("user");
        String who;
        if (user != null && !user.isEmpty()) {
            who = user;
        } else {
            who = "Welt";
        }

        req.setAttribute("who", who);

        final RequestDispatcher dispatcher = getServletConfig().getServletContext()
                .getRequestDispatcher("/WEB-INF/jsp/hello.jsp");
        dispatcher.include(req, resp);
    }
}
